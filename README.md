# audio-player

The audio player allows you to listen to music. Vuex is used as a state manager, it also uses TypeScript. <br/>
Right now the list of audio recordings is hardcoded in the state. For dynamic loading, you can connect the API.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
