export const urlToSong = (nameSong:string): string => {
  return require(`../assets/songs/${nameSong}`);
}

const convertTwoNumber = (num:number): string => {
  return num > 9 ? `${num}` : `0${num}`
}

export const convertTimeToMinAndSec = (time:any): string => {
  if (!time) return "00:00";

  const minute = convertTwoNumber(Math.floor(time / 60));
  const seconds = convertTwoNumber(Math.floor(time % 60))

  return `${minute}:${seconds}`;
}