import Vue from 'vue'
import Vuex, { StoreOptions } from 'vuex'

import { song } from "@/store/song";

Vue.use(Vuex);

const store: StoreOptions<any> = {
  state: {},
  modules: {
    song
  }
};

export default new Vuex.Store(store);
