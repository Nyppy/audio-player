import { ActionTree } from "vuex";
import { SongState } from "@/store/song/types";
import { urlToSong } from "@/helpers/songs";

export const actions:ActionTree<SongState, any> = {
  loadListSongs({ commit, dispatch }): void {
    const data = [
      {
        id : 1,
        nameSong: "Over Paris, A Balloon",
        artist: "Crash! Boom! Kapow!",
        nameFile: "Track-1.mp3",
        time: "04:28"
      },
      {
        id : 2,
        nameSong: "PRIMAVARA",
        artist: "harvestfred",
        nameFile: "Track-2.mp3",
        time: "04:42"
      },
      {
        id : 3,
        nameSong: "Y=MX+B",
        artist: "Peppy & The Firing Squad",
        nameFile: "Track-3.mp3",
        time: "03:22"
      },
      {
        id : 4,
        nameSong: "(94)",
        artist: "crashboomkapow",
        nameFile: "Track-4.mp3",
        time: "05:00"
      },
      {
        id : 5,
        nameSong: "07.02.21 - snake eyes",
        artist: "offkey",
        nameFile: "Track-5.mp3",
        time: "02:42"
      },
      {
        id : 6,
        nameSong: "Metal in my Body",
        artist: "AkA",
        nameFile: "Track-6.mp3",
        time: "03:27"
      },
    ];

    commit("SET_SONGS", data);
    dispatch("toggleSong", 0);
  },
  toggleSong({ getters, commit }, id:number): void {
    const song = getters.getListSongs[id];
    const src = urlToSong(song.nameFile);

    commit("SET_AUDIO_SRC", src);
    commit("SET_ACTIVE", id);
  },
  playSelectSong({ dispatch }, id:number): void {
    dispatch("pause");
    dispatch("toggleSong", id);
    dispatch("play");
  },
  play({ state, commit }): void {
    commit("SET_PAUSED", false);
    state.audio.play();
  },
  pause({ state, commit }): void {
    commit("SET_PAUSED", true);
    state.audio.pause();
  },
  toggleMuted({ state, commit }): void {
    const oldVal = Boolean(state.audio.muted);
    commit("SET_MUTED", !oldVal);
    state.audio.muted = !oldVal;
  },
  nextSong({ dispatch, commit, state, getters }): void {
    if (state.paused) dispatch("pause");

    const lengthSong = getters.getLengthSong;
    const activeSongId = getters.getActiveSongId;

    if (state.active >= lengthSong-1) {
      commit("SET_ACTIVE", 0);
      dispatch("toggleSong", 0);
    } else {
      commit("SET_ACTIVE", activeSongId+1);
      dispatch("toggleSong", activeSongId+1);
    }

    if (!state.paused) dispatch("play");
  },
  previousSong({ dispatch, commit, getters, state }): void {
    if (state.paused) dispatch("pause");

    const lengthSong = getters.getLengthSong;
    const activeSongId = getters.getActiveSongId;

    if (state.active === 0) {
      commit("SET_ACTIVE", lengthSong-1);
      dispatch("toggleSong", lengthSong-1);
    } else {
      commit("SET_ACTIVE", activeSongId-1);
      dispatch("toggleSong", activeSongId-1);
    }

    if (!state.paused) dispatch("play");
  },
  setVolume({ commit }, val:number): void {
    commit("SET_AUDIO_VOLUME", val);
  }
}