import { GetterTree } from "vuex";
import { SongState } from "@/store/song/types";

export const getters:GetterTree<SongState, any[]> = {
  getActiveSongId(state): number {
    return state.active;
  },
  getActiveSongData(state): any[] {
    const listSongs = state.songs;
    const activeSong = state.active;

    return listSongs[activeSong] || {};
  },
  getListSongs(state): any[] {
    return state.songs;
  },
  getLengthSong(state): number {
    return state.songs.length;
  },
  getPaused(state): boolean {
    return state.paused;
  },
  getAudio(state): any[] {
    return state.audio;
  },
  getMuted(state): boolean {
    return state.muted;
  },
  getCurrentTime(state): number {
    return Math.round(state.currentTime);
  }
}