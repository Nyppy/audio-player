import { Module } from "vuex"
import { SongState } from "@/store/song/types";
import { getters } from "@/store/song/getters";
import { mutations } from "@/store/song/mutations";
import { actions } from "@/store/song/actions";

const state:SongState = {
  songs: [],
  active: 0,
  audio: new Audio(),
  paused: true,
  muted: false,
  currentTime: 0
}

export const song:Module<SongState, any> = {
  state,
  getters,
  mutations,
  actions
}
