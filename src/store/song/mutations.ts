import { MutationTree } from "vuex";
import { SongState } from "@/store/song/types";

export enum SongMutations {
  SET_ACTIVE = "SET_ACTIVE",
  SET_SONGS = "SET_SONGS",
  SET_AUDIO_SRC = "SET_AUDIO_SRC",
  SET_AUDIO_VOLUME = "SET_AUDIO_VOLUME",
  SET_PAUSED = "SET_PAUSED",
  SET_MUTED = "SET_MUTED",
  SET_CURRENT_TIME = "SET_CURRENT_TIME",
}

export const mutations:MutationTree<SongState> = {
  [SongMutations.SET_ACTIVE](state, payload: number) {
    state.active = payload;
  },
  [SongMutations.SET_SONGS](state, payload: any[]) {
    state.songs = payload;
  },
  [SongMutations.SET_AUDIO_SRC](state, payload: string) {
    state.audio.src = payload;
  },
  [SongMutations.SET_AUDIO_VOLUME](state, payload: number) {
    state.audio.volume = payload;
  },
  [SongMutations.SET_PAUSED](state, payload: boolean) {
    state.paused = payload;
  },
  [SongMutations.SET_MUTED](state, payload: boolean) {
    state.muted = payload;
  },
  [SongMutations.SET_CURRENT_TIME](state, payload: number) {
    state.currentTime = payload;
  }
}