export interface SongState {
  songs: any[],
  active: number,
  audio: any,
  paused: boolean,
  muted: boolean,
  currentTime: number,
}